/**
 * Created by Emanuel on 5/8/2015.
 */


//Login and Register

function showform(string){
    if(string.trim() == "Login"){
        document.getElementById("login").style.display = "none";
        document.getElementById("register").style.display = "none";
        document.getElementById("userName_row").style.visibility = "hidden";
        document.getElementById("username_input").required = false;


    }else{
        document.getElementById("login").style.display = "none";
        document.getElementById("register").style.display = "none";
        document.getElementById("userName_row").style.visibility = "visible";
        document.getElementById("username_input").required = true;


    }

    document.getElementById("login_register").style.display = "block";
    document.getElementById("result").innerHTML = string.trim() + "<hr>";
    var result = document.getElementById("result").style;
    result.paddingTop = "10px";
    document.getElementById("login_register_button").innerHTML = string.trim();
}


function logout(){
    var string = "test";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementsByClassName("content")[0].innerHTML = xmlhttp.responseText;
            window.location.reload();
        }
    }
    xmlhttp.open("GET","server/Exec.php?text=" + string,true);
    xmlhttp.send();
}

function login(){
    var action = "login";
    var email = document.getElementById("email_input").value.trim();
    var password = document.getElementById("password_input").value.trim();

    var l = new XMLHttpRequest();
    l.onreadystatechange = function(){
        if(l.readyState == 4 && l.status == 200){
            document.getElementById("errorxsucces").innerHTML = l.responseText;
            if(document.getElementById("error_query").innerHTML == "User not found. Try again!"){

            }else if(document.getElementById("error_query").innerHTML == "Login succesful!"){
                window.location.reload();
            }

        }
    }
    l.open("POST","server/Actions.php?action=" + action + "&email=" + email + "&password=" + password ,true);
    l.send();

    return true;
}

function register(){
    var action = "register";

    var username = document.getElementById("username_input").value.trim();
    var email = document.getElementById("email_input").value.trim();
    var password = document.getElementById("password_input").value.trim();

    var r = new XMLHttpRequest();
    r.onreadystatechange = function(){
        if(r.readyState == 4 && r.status == 200){
            document.getElementById("errorxsucces").innerHTML = r.responseText;
            if(document.getElementById("error_query").innerHTML == "Already exists an user with this username. Choose another username!"){


            }else if(document.getElementById("error_query").innerHTML == "New user created succesfully!"){
                window.location.reload();
            }
        }
    }
    r.open("POST","server/Actions.php?action=" + action + "&username=" + username + "&email=" + email + "&password=" + password ,true);
    r.send();

    return true;
}

function form_validation(){
    var result;
    if(document.getElementById("result").innerHTML.trim() == "Login<hr>"){
        result = login();
    }else{
        result = register();
    }
    return false;
}

function testAJAX(){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function(){
        if(x.readyState == 4 && x.status == 200){
            document.getElementById("test").innerHTML = x.responseText;
        }
    }
    x.open("GET","server/Actions.php",true);
    x.send();
}


//Getting the score
//
//function getscore(){
//    var action = "getscore";
//
//    var scorerequest = new XMLHttpRequest();
//    scorerequest.onreadystatechange = function(){
//        if(scorerequest.readyState == 4 && scorerequest.status == 200){
//            document.getElementById("user_scores").innerHTML = scorerequest.responseText;
//        }
//    }
//    scorerequest.open("POST","server/Scoreactions.php?action=" + action ,true);
//    scorerequest.send();
//}
