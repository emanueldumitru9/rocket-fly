<?php
    session_start();
    include("server/Users.php");
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Rocket Fly </title>

     <link href="styles/style.css" type="text/css" rel="stylesheet">
    <link rel="shortcut icon" href="images/logo.jpg">
	<link rel="stylesheet" href="styles.css" type="text/css">

    <script src="scripts/my_script.js"></script>
	
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<script src="script.js"></script>

</head>
<body>
<div class="wrapper">
    <div class="header">
        <div id="cssmenu" class = 'align-center'>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="about.html">About</a></li>
                <li class="active"><a href="account.php">Account</a></li>
                <li><a href="play.html">Play game </a></li>
                <li><a href="highscores.html">Highscores</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
        <div id="float">
				<img id="header" src="images/RocketSite.png" alt="header_image">
			</div>
    </div>
    <div class="content">
        <?php if (!empty($_POST)):

                $user = new Users($_POST['username'],$_POST['password'],$_POST['email']);

                $_SESSION["username"] = $user->username;
                $_SESSION["password"] = $user->password;
                $_SESSION["email"] = $user->email;

         ?>

        <?php elseif(!count($_SESSION)): ?>
            <form action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?> method="post" onsubmit="return form_validation()" id="myform">
                <nav id="loginXregister">
                    <span id="login" onclick="showform(this.innerHTML)"> Login </span>
                    <span id="register" onclick="showform(this.innerHTML)"> Register </span>
                </nav>
                <span id="result">

                </span>
                <br>
                <section id="login_register">
                    <br>
                    <table>
                        <tr id="userName_row">
                            <td> UserName </td>
                            <td> <input type="text" name="username" id="username_input"></td>

                        </tr>
                        <tr>
                            <td> Email </td>
                            <td> <input type="text" name="email" id="email_input" required> </td>

                        </tr>
                        <tr>
                            <td> Password </td>
                            <td> <input type="password" name="password" id="password_input" required> </td>

                        </tr>
                    </table>
                    <button type="submit" id="login_register_button">  </button>
                    <br>
                    <br>
                    <div id="errorxsucces">

                    </div>
                </section>
            </form>

        <?php elseif(count($_SESSION)>0):
            echo "<h1> Welcome " . $_SESSION["username"] . "! </h1>";
            echo "<hr>";

            echo "<h4> My account </h4>";
            echo "<br>";
            echo "<div id='user_data'>";
            echo "<span> Username : </span>" . $_SESSION["username"] . "<br>";
            echo "<span> Email    : </span>" . $_SESSION["email"] . "<br>";
            echo "<span> Highscore    : </span>";
            if(isset($_SESSION['score'])){
                echo $_SESSION['score']." points";
            }else{
                echo "You didn't play yet.";
            }
            echo "<br>";


            echo '<button onclick="logout()"> Logout </button>';
            echo "</div>";
        ?>

        <?php endif; ?>
    </div>

</div>
<div class="footer">  &copy;Rocket Fly 2015
</div>


</body>
</html>