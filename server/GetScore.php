<?php
    session_start();
    include("Database.php");

    $score = $_POST["Score"];
    $_SESSION["score"]  = $score;
    function insert_score(){
        if(isset($_SESSION["email"]) && isset($_SESSION["password"]) && isset($_SESSION["score"])){
            $my_database = new Database();
            $my_database->connect_to_database();

            $email = trim($_SESSION["email"]);
            $password = trim($_SESSION["password"]);
            $score = trim($_SESSION["score"]);

            $_SESSION["score"]= $my_database->add_score($email,$password,$score);


            $my_database->disconnect_from_database();
        }else{
            session_unset();
            session_destroy();
        }
    }
    insert_score();


?>