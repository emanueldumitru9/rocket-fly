<?php
class Database
{
    private $host = "mysql13.000webhost.com";
    private $database = "a2153399_Rocket";
    private $user = "a2153399_EMY";
    private $password = "Ch@let9";
    private $connection;

    function connect_to_database()
    {
        $this->connection = mysqli_connect($this->host,$this->user,$this->password,$this->database);
        if (!$this->connection) {
            die("Connection failed: " . mysqli_connect_error());
        }
    }

    function disconnect_from_database()
    {
        mysqli_close($this->connection);
    }
    function selecthighscores($username){
        $sql = "SELECT username, score FROM highscores ORDER BY score DESC LIMIT 10 ";
        $result = $this->connection->query($sql);

        if ($result->num_rows > 0) {
            $index = 1;
            echo "<table id='highscore_table'>";
            echo "<tr>";
            echo "<th> Position </th>";
            echo "<th> Username </th>";
            echo "<th> Score </th>";
            echo "</tr>";
            while($row = $result->fetch_assoc()){
                if($username == $row['username']) {
                    echo "<tr id='particular_score'>";
                }else {
                    echo "<tr>";
                }
                echo "<td>". $index. ".</td>";
                echo "<td>". $row['username'] . "</td>";
                echo "<td>". $row['score']."</td>";
                echo "</tr>";
                $index++;
            }
            echo "</table>";
        } else {
            echo "There are no scores stored in database!";
        }
    }

    /**
     * @param $username
     * @param $email
     * @param $password
     */
    function insert_to_database_user($username, $email, $password){
        $password = md5($password);
        $sql = "INSERT INTO users(username, email, password) VALUES ('$username','$email','$password')";
        $result = $this->connection->query($sql);

        if($result === TRUE){
            echo "<span id='error_query' style='color:azure;'>New user created succesfully!</span> <br> ";
        }else{
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }

    function check_if_exists($username, $email){
        $sql = "SELECT username, email FROM users WHERE username='$username' or email='$email' OR (username='$username' AND  email ='$email')";
        $result = $this->connection->query($sql);
        if ($result->num_rows > 0)
            return true;
        return false;
    }

    /**
     * @param $email
     * @param $password
     * @return array|bool
     */
    function check_login($email, $password){
        $password = md5($password);
        $sql = "SELECT username, email, password, score FROM users WHERE email='$email' AND password ='$password'";
        $result = $this->connection->query($sql);
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $data = array($row['username'], $row['email'], $row['password'], $row['score']);
            return $data;
        }
        return false;
    }

    /**
     * @param $email
     * @param $password
     * @param $score
     */
    function add_score($email, $password, $score){

        $get_myscore = "select score from users where email ='$email'";
        $result = $this->connection->query($get_myscore);
        $row = $result->fetch_assoc();
        $old_score = $row['score'];
        if($old_score != null) {
            if ($old_score > $score)
                $score = $old_score;
        }



        $sql = "update users set score = '$score' where email = '$email' and (score is null or $score > score)";
        $result = $this->connection->query($sql);


        $get_name = "select user_id, username from users where email='$email'";
        $res = $this->connection->query($get_name);
        $row = $res->fetch_assoc();
        $username = $row['username'];
        $user_id = $row['user_id'];


        $truncate_highscores = "TRUNCATE table highscores";
        $res = $this->connection->query($truncate_highscores);

        $insert_highscore = "insert into highscores(user_id,username,score) select user_id, username, score from users where score is not null";
        $res = $this->connection->query($insert_highscore);
        return $score;
    }

    //function to be used for unit testing

    function UnitTestConnection(){
        $this->connection = mysqli_connect($this->host,$this->user,$this->password,$this->database);
        return $this->connection;
    }

    function UnitTestDisconnect(){
       return mysqli_close($this->connection);
    }
}


?>