<?php
/**
 * Created by PhpStorm.
 * User: Emanuel
 * Date: 5/26/2015
 * Time: 2:09 AM
 */

require_once("../server/ForTesting.php");
require_once("../server/Database.php");
require_once('../server/Users.php');
require_once("PHPUnit/Autoload.php");
class Testuserexists extends PHPUnit_Framework_TestCase {
    private $database;
    private $email;
    public  function __construct(){
        $this->database = new Database();
        $this->email = "emanuel.dumitru9@gmail.com";
        $this->username = "Emanuel";
    }
    public function testUserExists(){
        $this->assertTrue($this->database->UnitTestCheckifExists($this->email));
    }

    public function testUserScoreIsPositive(){
        $score = $this->database->UnitTestCheckPositiveScore($this->email);
        $this->assertGreaterThanOrEqual(0,$score);
    }

    public function testUserEmailIsValidFormat(){
        $this->assertRegExp("/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/",$this->email);
    }

    public function testUserIsInRankings(){
        $rankings = $this->database->UnitTestCheckUserIsInRankings($this->username);
        $this->assertContains($this->username,$rankings);
    }
}
