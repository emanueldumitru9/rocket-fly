<?php
/**
 * Created by PhpStorm.
 * User: Emanuel
 * Date: 5/26/2015
 * Time: 1:50 AM
 */

require_once("../server/Database.php");
class DatabaseTest extends PHPUnit_Framework_TestCase {
    private $my_database;
    public function __construct(){
        $this->my_database = new Database();
    }

    public function testConnect(){
        $this->assertTrue($this->my_database->UnitTestConnection());
    }

    public function testDisconnect(){
        $this->assertTrue($this->my_database->UnitTestDisconnect());
    }
}
