<?php
/**
 * Created by PhpStorm.
 * User: Emanuel
 * Date: 5/26/2015
 * Time: 3:54 AM
 */

class TestGameFileExists extends PHPUnit_Framework_TestCase {
    private $filename;
    public function __construct(){
        $this->filename = "rocketfly.unity3d";
    }

    public function testGameFileExists(){
        $path="../";
        $this->assertFileExists($path.$this->filename);
    }
}
